There's roughly three types of errors:

1. __Expected errors__ - eg. "URL is unreachable" for a link validity checker. You should handle these in your code at the top-most level where it is practical to do so.
2. __Unexpected errors__ - eg. a bug in your code. These should crash your process (yes, really), they should be logged and ideally e-mailed to you, and you should fix them right away. You should never catch them for any purpose other than to log the error, and even then you should make the process crash.
3. __User-facing errors__ - not really in the same category as the above two. While you can represent them with error objects (and it's often practical to do so), they're not really errors in the programming sense - rather, they're user feedback. When represented as error objects, these should only ever be handled at the top-most point of a request - in the case of Express, that would be the error-handling middleware that sends a HTTP status code and a response.

---

> Would I still need to use try/catch if I use promises?

_Sort of._ Not the usual `try`/`catch`, but eg. Bluebird has a `.try` and `.catch` equivalent. It works like synchronous `try`/`catch`, though - errors are propagated upwards automatically so that you can handle them where appropriate.

Bluebird's `try` isn't identical to a standard JS `try` - it's more a 'start using Promises' thing, so that you can also wrap synchronous errors. That's the magic of Promises, really - they let you handle synchronous and asynchronous errors/values like they're one and the same thing.

[Here](https://gist.github.com/joepie91/f6a56acdae303e90e44a) is a relatively complex example, that uses a custom 'error filter' (predicate) function, because filesystem errors have a name but not a special error type. The error filtering is only available in Bluebird, by the way - 'native' Promises don't have the filtering.